﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Selector))]
public class SelectorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        Selector selector = (Selector)target;
        DrawDefaultInspector();

        EditorGUILayout.BeginHorizontal();
        if(GUILayout.Button("透明+關閉"))
        {
            selector.ChangeToTransparent(selector.GameObjectList);
            selector.CloseObject(selector.GameObjectList);
        }
        if (GUILayout.Button("實色+啟動"))
        {
            selector.ChangeSolidColor(selector.GameObjectList);
            selector.OpenObject(selector.GameObjectList);
        }

        EditorGUILayout.EndHorizontal();
        if(GUILayout.Button("選擇以上物件"))
        {
            Selector.ChooseSceneObject(selector.GameObjectList);
        }
    }

}

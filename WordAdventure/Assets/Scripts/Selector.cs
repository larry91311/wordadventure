﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Selector : MonoBehaviour
{
    public string Mark = "註解";
    public GameObject[] GameObjectList;
    public void OpenObject(GameObject project)  //project=項目
    {
        project.SetActive(true);
    }

    public void OpenObject(GameObject [] GameObjectList) //重載
    {
        foreach(var project in GameObjectList)
        {
            if(project!= null)
            {
                OpenObject(project);
            }
        }
    }
    public void CloseObject(GameObject project)
    {
        project.SetActive(false);
    }
    public void CloseObject(GameObject[] GameObjectList) //重載
    {
        foreach (var project in GameObjectList)
        {
            if (project != null)
            {
                CloseObject(project);
            }
        }
    }
    public void ChangeTransparency(GameObject project,float Transparency)  //Tool=顏色渲染器,ColoR=顏色
    {
        SpriteRenderer Tool = project.GetComponent<SpriteRenderer>();
        Color ColoR = Tool.color;
        ColoR.a = Transparency;
        Tool.color = ColoR;

    }
    public void ChangeToTransparent(GameObject project)
    {
        ChangeTransparency(project, 0);
    }
    public void ChangeToTransparent(GameObject [] GameObjectList)
    {
        foreach(var project in GameObjectList)
        {
            if(project!=null)
            {
                ChangeToTransparent(project);
            }
        }
        
    }
    public void ChangeSolidColor(GameObject project) //改實色
    {
        ChangeTransparency(project, 1);
    }
    public void ChangeSolidColor(GameObject[] GameObjectList)
    {
        foreach (var project in GameObjectList)
        {
            if (project != null)
            {
                ChangeSolidColor(project);
            }
        }

    }
    public static void ChooseSceneObject(GameObject[] GameObjectList)
    {
        Selection.objects = GameObjectList;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
